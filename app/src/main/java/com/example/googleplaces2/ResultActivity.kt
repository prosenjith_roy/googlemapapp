package com.example.googleplaces2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val address = intent.getParcelableExtra<Address>("address")

        val textView = findViewById<TextView>(R.id.textView3)

        textView.text=address.toString()
    }
}
