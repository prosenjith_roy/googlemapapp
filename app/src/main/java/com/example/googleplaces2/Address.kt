package com.example.googleplaces2

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(
    val location:String?,
    val homeAddress: String?,
    val city : String?,
    val pinCode : String?
):Parcelable